[![Build Status](https://drone.vivitech-bnu.com.br/api/badges/vivipaynesp/rdfreact/status.svg)](https://drone.vivitech-bnu.com.br/vivipaynesp/rdfreact)

#### RDF Frontend
Install dependencies (if not using docker) or run development mode (see development):

    $ make yarn-install
    $ make resetOwner

To use docker:

    $ make docker-start APP_ENV=prod # production
    $ make docker-start APP_ENV=dev # development 
    $ make docker-stop # stop all containers

#### Development
To load built-in server (requires yarn installed locally):

    $ make server-start
