help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  clean                                  Clean directories for reset"
	@echo "  yarn-install                           Install NPM dependencies with yarn"

init:
	@if [ "$(APP_ENV)" = "prod" ]; then \
		cp -n $(shell pwd)/.env.prod $(shell pwd)/.env 2> /dev/null; \
		cp -n $(shell pwd)/docker-compose.yml.prod $(shell pwd)/docker-compose.yml 2> /dev/null; \
	else \
		cp -n $(shell pwd)/.env.dist $(shell pwd)/.env 2> /dev/null; \
		cp -n $(shell pwd)/docker-compose.yml.dist $(shell pwd)/docker-compose.yml 2> /dev/null; \
	fi

clean:
	@rm -Rf node_modules
	@rm -Rf package-lock.json
	@rm -Rf .env
	@rm -Rf build
	@rm -Rf docker-compose.yml

yarn-install:
	@docker run --rm -v $(shell pwd)/:/app -w /app node:latest yarn install
	@docker run --rm -v $(shell pwd)/:/app -w /app node:latest yarn run build

server-start: clean init yarn-install resetOwner
	yarn run start

docker-start: clean init yarn-install resetOwner
	docker-compose up -d

docker-stop: resetOwner
	@docker-compose stop
	@make clean

resetOwner:
	@$(shell sudo chown -Rf $(shell id -u):$(shell id -g) "$(shell pwd)/" 2> /dev/null)

.PHONY: clean init
