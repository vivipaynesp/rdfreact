import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import RegistrationForm from '../registrationform';
import LoginForm from '../loginform';
import {withStyles} from '@material-ui/core/styles'
import styles from './app-grid-style'
import {useSelector} from "react-redux";

export default withStyles(styles)(({classes, onGridRegister, onSubmit}) => {

    const isLogged = useSelector(state => state.isLogged);

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={2}>
                </Grid>
                <Grid item xs={4}>
                    <div className={classes.paper}>
                        <LoginForm
                            classes={classes}
                            onSubmit={onSubmit}
                        />
                    </div>
                    Is logged in {isLogged ? 'yes' : 'no'}
                </Grid>
                <Grid item xs={4}>
                    <div className={classes.paper}>
                        <RegistrationForm
                            classes={classes}
                            onRegister={onGridRegister}
                        />
                    </div>
                </Grid>
                <Grid item xs={2}>
                </Grid>
            </Grid>
        </div>
    );
})
