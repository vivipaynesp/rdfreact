import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

class TextFieldGeneral extends Component {
    render() {
        const {isRequired, handleChange, ...props} = this.props;

        return (
            <TextField
                required={isRequired}
                fullWidth
                id={props.name}
                onChange={handleChange}
                {...props}
            />
        );
    }
}

TextFieldGeneral.propTypes = {
    autoComplete: PropTypes.string,
    InputLabelProps: PropTypes.object,
    label: PropTypes.string,
    margin: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.string,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    isRequired: PropTypes.bool,
    value: PropTypes.string,
    handleChange: PropTypes.func,
};

export default TextFieldGeneral;