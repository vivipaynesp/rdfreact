import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";

const styles = theme => ({
    button: {
        margin: theme.spacing(3, 0, 2),
    },
});

class ButtonGeneral extends Component {
    render() {
        const {classes, message, type, value} = this.props;
        return (
            <Button
                type={type}
                value={value}
                fullWidth
                variant="contained"
                color="primary"
                className={classes.button}
            >
                {message}
            </Button>
        );
    }
}

ButtonGeneral.propTypes = {
    message: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonGeneral);