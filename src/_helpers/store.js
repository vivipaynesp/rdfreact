import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../_reducers';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';

//#region Constants
const middlewares = [thunkMiddleware];
const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

if (process.env.NODE_ENV === `development`) {
    const { logger } = require(`redux-logger`);
    middlewares.push(logger);
}

const enhancer = composeEnhancers(
    applyMiddleware(...middlewares)
);
//#endregion

//#region Exports
export const store = createStore(
    rootReducer,
    enhancer
);
//#endregion
