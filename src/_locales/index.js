import pt from './default.json';
import en from './default.en.json';
import es from './default.es.json';
import signInPt from '../SignIn/locales/default.json';
import signInEn from '../SignIn/locales/default.en.json';
import signInEs from '../SignIn/locales/default.es.json';
import homePt from '../HomePage/locales/default';
import homeEn from '../HomePage/locales/default.en.json';
import homeEs from '../HomePage/locales/default.es.json';
import signUpPt from '../SignUp/locales/default.json';
import signUpEn from '../SignUp/locales/default.en.json';
import signUpEs from '../SignUp/locales/default.es.json';

pt['pt-BR'].signin = signInPt['pt-BR'];
en['en'].signin = signInEn['en'];
es['es'].signin = signInEs['es'];

pt['pt-BR'].home = homePt['pt-BR'];
en['en'].home = homeEn['en'];
es['es'].home = homeEs['es'];

pt['pt-BR'].signup = signUpPt['pt-BR'];
en['en'].signup = signUpEn['en'];
es['es'].signup = signUpEs['es'];

export {
    pt,
    es,
    en,
};