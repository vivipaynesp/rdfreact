import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {userActions} from "../_actions";
import ButtonGeneral from "../_components/button/button.general";
import TextFieldGeneral from "../_components/textfield/textfield.general";
import Copyright from "../_components/copyright/";
import styles from "./styles";
import {withTranslation} from 'react-i18next';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TranslateIcon from '@material-ui/icons/Translate';
import i18next from "i18next";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
import {LANGUAGES_LABEL} from '../_constants';
import NoSsr from '@material-ui/core/NoSsr';

class SignIn extends Component {
    constructor(props) {
        super(props);
        // reset login status
        this.props.logout();
        this.state = {
            email: '',
            password: '',
            submitted: false,
            lng: i18next.language,
            languageMenu: null
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleLanguageIconClick = this.handleLanguageIconClick.bind(this);
        this.handleLanguageMenuClose = this.handleLanguageMenuClose.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        this.setState({[name]: value});
    }

    handleSubmit(event) {
        if (event) {
            event.preventDefault();
        }

        this.setState({submitted: true});
        const {email, password} = this.state;

        if (email && password) {
            this.props.login(email, password);
            // this.props.sendFlashMessage('You win!', 'alert-success');
        }
    }

    handleClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleLanguageIconClick = event => {
        this.setState({languageMenu: event.currentTarget});
    };

    handleLanguageMenuClose = (lng) => {
        this.setState({languageMenu: null});
        this.setState({lng: lng}, () => {
            i18next.changeLanguage(lng);
        });
    };

    render() {
        const {classes, loggingIn, t} = this.props;
        const {email, languageMenu, lng, password, submitted} = this.state;

        return (
            <React.Fragment>

                {/* Menu */}
                <AppBar position="static" color="primary" elevation={0}>
                    <Toolbar variant="dense">
                        <Tooltip title={t('defaults.changeLanguage')} enterDelay={300}>
                            <Button
                                color="inherit"
                                aria-owns={languageMenu ? 'language-menu' : undefined}
                                aria-haspopup="true"
                                aria-label={t('defaults.changeLanguage')}
                                onClick={this.handleLanguageIconClick}
                                data-ga-event-category="AppBar"
                                data-ga-event-action="language"
                                className={classes.menuButton}
                            >
                                <TranslateIcon/>
                                <span className={classes.language}>
                                    {lng === 'aa' ? 'Translating' : LANGUAGES_LABEL.filter(language => language.code === lng)[0].text}
                                </span>
                                <ExpandMoreIcon fontSize="small"/>
                            </Button>
                        </Tooltip>
                        <NoSsr>
                            <Menu
                                id="language-menu"
                                anchorEl={languageMenu}
                                open={Boolean(languageMenu)}
                                onClose={this.handleLanguageMenuClose}
                            >
                                {LANGUAGES_LABEL.map(language => (
                                    <MenuItem
                                        data-no-link="true"
                                        key={language.code}
                                        selected={lng === language.code}
                                        onClick={this.handleLanguageMenuClose.bind(this, language.code)}
                                        lang={language.code}
                                    >
                                        {language.text}
                                    </MenuItem>
                                ))}
                            </Menu>
                        </NoSsr>
                    </Toolbar>
                </AppBar>

                {/* Main container */}
                <Container component="main" maxWidth="xs">
                    <CssBaseline/>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            {t('signin.signIn')}
                        </Typography>
                        <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
                            <TextFieldGeneral
                                autoComplete="email"
                                label={t('signin.email')}
                                margin="normal"
                                name="email"
                                type="email"
                                value={email}
                                handleChange={this.handleChange}/>

                            {
                                submitted && !email &&
                                <FormHelperText className={classes.helper}>Your username is required!</FormHelperText>
                            }

                            <TextFieldGeneral
                                autoComplete="current-password"
                                label={t('signin.password')}
                                margin="normal"
                                name="password"
                                type="password"
                                value={password}
                                handleChange={this.handleChange}/>

                            {
                                submitted && !password &&
                                <FormHelperText className={classes.helper}>Your password is required!</FormHelperText>
                            }

                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary"/>}
                                label={t('signin.rememberMe')}
                            />

                            <ButtonGeneral type="submit" value="Submit" message={t('signin.signIn')}/>

                            {
                                loggingIn && <Avatar
                                    src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="/>
                            }

                            <Grid container>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        {t('signin.forgotPassword')}
                                    </Link>
                                </Grid>
                                <Grid item>
                                    <Link href="/register" variant="body2">
                                        {t('signin.noAccount')}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>

                    {/* Footer */}
                    <footer className={classes.footer}>
                        <Container maxWidth="lg">
                            <Copyright />
                        </Container>
                    </footer>
                </Container>
            </React.Fragment>
        );
    }
}

SignIn.propTypes = {
    login: PropTypes.func,
    logout: PropTypes.func,
    classes: PropTypes.object,
    loggingIn: PropTypes.bool,
};

function mapState(state) {
    const {loggingIn} = state.authentication;
    return {loggingIn};
}

const actionCreators = {
    login: userActions.login,
    logout: userActions.logout,
};

export default withStyles(styles)(connect(mapState, actionCreators)(withTranslation('common')(SignIn)));
