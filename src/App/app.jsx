import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Router, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {CssBaseline} from "@material-ui/core";
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles'; // v1.x
import {history} from '../_helpers';
import {alertActions} from '../_actions';
import {PrivateRoute} from '../_components/privateroute';
import SnackbarGeneral from "../_components/snackbar";
import {HomePage} from '../HomePage';
import SignIn from '../SignIn';
import SignUp from '../SignUp';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: '#3f50b5',
            dark: '#002884',
            contrastText: '#fff',
        },
        secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: '#000',
        },
    },
});

class App extends Component {
    constructor(props) {
        super(props);
        history.listen(() => {
            // clear alert on location change
            setTimeout(() => {
                this.props.clearAlerts();
            }, 4000);
        });
    }

    render() {
        const {alert} = this.props;

        return (
            <MuiThemeProvider theme={theme}>
                <React.Fragment>
                    <CssBaseline/>

                    {
                        alert.message && <SnackbarGeneral variant={alert.type} message={alert.message}/>
                    }

                    <Router history={history}>
                        <div>
                            <PrivateRoute exact path="/" component={HomePage}/>
                            <Route path="/login" component={SignIn}/>
                            <Route path="/register" component={SignUp}/>
                        </div>
                    </Router>
                </React.Fragment>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = {
    alert: PropTypes.object,
    clearAlerts: PropTypes.func,
};

function mapState(state) {
    const {alert} = state;
    return {alert};
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export {connectedApp as App};