import {register as registerService} from '../_services/register.service'
import {actionTypes} from '../_constants/register.constants'
import {history} from '../_helpers';
import {alertActions} from './alert.actions';

export const register = (payload) => {
    return dispatch => {
        dispatch({type: actionTypes.REGISTER_REQUEST});

        registerService(payload)
            .then(() => {
                dispatch({type: actionTypes.REGISTER_SUCCESS});
                dispatch(alertActions.success('User created successfully'));
                history.push('/login');

            })
            .catch(error => {
                dispatch({type: actionTypes.REGISTER_FAILURE, payload: error});
                if (
                    error.response &&
                    error.response.data &&
                    error.response.data.errors
                ) {
                    if (error.response.data.errors.detail) {
                        if (error.response.data.errors.detail.message) {
                            dispatch(alertActions.error(error.response.data.errors.detail.message));
                        } else {
                            dispatch(alertActions.error(error.response.data.errors.detail));
                        }
                    } else if (Array.isArray(error.response.data.errors)) {
                        const errors = error.response.data.errors.reduce(
                            (prev, {detail = {property_path: '', message: ''}}) => (
                                `${prev}${detail.property_path} - ${detail.message}<br/>`
                            ),
                            ''
                        );
                        dispatch(alertActions.error(errors));
                    } else {
                        dispatch(alertActions.error(error.message));
                    }
                } else {
                    dispatch(alertActions.error(error.message));
                }
            })
    }
};