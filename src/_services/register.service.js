import api from './api'

export async function getCountries(page) {
    const response = await api.get(`countries?page=${page}`);

    return response.data;
}

export async function getStates(countryId) {
    const response = await api.get(`countries/${countryId}`);

    if (response.data) {
        return response.data.state
    }

    return [];
}

export async function getCities(stateId) {
    const response = await api.get(`states/${stateId}`);

    if (response.data) {
        return response.data.city
    }

    return [];
}

export async function validateSponsorId(sponsorId) {
    let sponsorValidation = null;

    try {
        sponsorValidation = await api.get(`sponsor/${sponsorId}`);
    } catch (error) {
        return false;
    }

    if (typeof sponsorValidation === 'undefined' || sponsorValidation === null) {
        return false;
    } else {
        return sponsorValidation.data.data;
    }
}

export async function register(data) {
    const response = await api.post('register', data);

    return response.data;
}
