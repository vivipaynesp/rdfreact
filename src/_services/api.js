import axios from 'axios'

// import {authHeader} from '../_helpers';

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL + '/api',
    // headers: authHeader()
});

export default api;
