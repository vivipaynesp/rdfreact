// import React from 'react';
// import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
//
import {userActions} from '../_actions';
// import PrimarySearchAppBar from "../_components/PrimarySearchAppBar/PrimarySearchAppBar";
// import SecondaryAppBar from "../_components/submenubar/app-submenubar";
// import {CssBaseline} from "@material-ui/core";
// import {Home} from "@material-ui/icons";
//
// class HomePage extends React.Component {
//     componentDidMount() {
//         this.props.getUsers();
//     }
//
//     handleDeleteUser(id) {
//         return (e) => this.props.deleteUser(id);
//     }
//
//     render() {
//         const {user, users} = this.props;
//         return (
//             <React.Fragment>
//                 <CssBaseline/>
//                 <PrimarySearchAppBar/>
//                 <SecondaryAppBar/>
//
//

//             </React.Fragment>
//         );
//     }
// }
//
// HomePage.propTypes = {
//     user: PropTypes.object,
//     users: PropTypes.object,
// };
//
// function mapState(state) {
//     const {users, authentication} = state;
//     const {user} = authentication;
//     return {user, users};
// }
//
// const actionCreators = {
//     getUsers: userActions.getAll,
//     deleteUser: userActions.delete
// }
//
// const connectedHomePage = connect(mapState, actionCreators)(HomePage);
// export {connectedHomePage as HomePage};

import React, {Component} from 'react';
import PropTypes from "prop-types";
import clsx from 'clsx';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Box from "@material-ui/core/Box";
// import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
// import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {MainListItems, SecondaryListItems} from '../_components/List';
// import Chart from './Chart';
// import Deposits from './Deposits';
// import Orders from './Orders';
import withStyles from "@material-ui/core/styles/withStyles";
import Copyright from "../_components/copyright/";
import logo from "../_assets/images/logo.png";
import styles from "./styles";
import {withTranslation} from 'react-i18next';
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import {LANGUAGES_LABEL} from "../_constants";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import NoSsr from '@material-ui/core/NoSsr';
import i18next from "i18next";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TranslateIcon from '@material-ui/icons/Translate';

class HomePage extends Component {
    constructor(props) {
        super(props);

        // this.props.logout();

        // validate route path is same username or logout

        this.state = {
            open: false,
            lng: i18next.language,
            languageMenu: null
        };

        this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
        this.handleDrawerClose = this.handleDrawerClose.bind(this);
        this.handleLanguageIconClick = this.handleLanguageIconClick.bind(this);
        this.handleLanguageMenuClose = this.handleLanguageMenuClose.bind(this);
    }

    handleDrawerOpen() {
        this.setState({
            open: true,
        });
    };

    handleDrawerClose() {
        this.setState({
            open: false,
        });
    };

    componentDidMount() {
        this.props.getUser();
    }

    handleLanguageIconClick = event => {
        this.setState({languageMenu: event.currentTarget});
    };

    handleLanguageMenuClose = lng => {
        this.setState({languageMenu: null});
        this.setState({lng: lng}, () => {
            i18next.changeLanguage(lng);
        });
    };

    render() {
        const {classes, t, user} = this.props;
        const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
        const {languageMenu, lng} = this.state;

        return (
            <div className={classes.root}>
                <CssBaseline/>
                <AppBar position="absolute" color="inherit"
                        className={clsx(classes.appBar, this.state.open && classes.appBarShift)}>
                    <Toolbar className={classes.toolbar}>
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.handleDrawerOpen}
                            className={clsx(classes.menuButton, this.state.open && classes.menuButtonHidden)}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <img alt="Remy Sharp" src={logo} className={classes.logo}/>
                        <Box className={classes.spacer}/>

                        {/*<Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>*/}
                        {/*    Dashboard*/}
                        {/*</Typography>*/}

                        <Tooltip title={t('defaults.changeLanguage')} enterDelay={300}>
                            <Button
                                color="inherit"
                                aria-owns={languageMenu ? 'language-menu' : undefined}
                                aria-haspopup="true"
                                aria-label={t('defaults.changeLanguage')}
                                onClick={this.handleLanguageIconClick}
                                data-ga-event-category="AppBar"
                                data-ga-event-action="language"
                                className={classes.languageMenu}
                            >
                                <TranslateIcon/>
                                <span className={classes.language}>
                {lng === 'aa'
                    ? 'Translating'
                    : LANGUAGES_LABEL.filter(language => language.code === lng)[0].text}
              </span>
                                <ExpandMoreIcon fontSize="small"/>
                            </Button>
                        </Tooltip>
                        <NoSsr>
                            <Menu
                                id="language-menu"
                                anchorEl={languageMenu}
                                open={Boolean(languageMenu)}
                                onClose={this.handleLanguageMenuClose}
                            >
                                {LANGUAGES_LABEL.map(language => (
                                    <MenuItem
                                        data-no-link="true"
                                        key={language.code}
                                        selected={lng === language.code}
                                        onClick={this.handleLanguageMenuClose.bind(this, language.code)}
                                        lang={language.code}
                                    >
                                        {language.text}
                                    </MenuItem>
                                ))}
                            </Menu>
                        </NoSsr>

                        <IconButton color="inherit">
                            <Badge badgeContent={4} color="secondary">
                                <NotificationsIcon/>
                            </Badge>
                        </IconButton>

                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: clsx(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
                    }}
                    open={this.state.open}
                >
                    <div className={classes.toolbarIcon}>
                        <IconButton onClick={this.handleDrawerClose}>
                            <ChevronLeftIcon/>
                        </IconButton>
                    </div>
                    <Divider/>
                    <List>
                        <MainListItems/>
                    </List>
                    <Divider/>
                    <List>
                        <SecondaryListItems/>
                    </List>
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer}/>
                    <Container maxWidth={false} className={classes.container}>
                        <Grid container spacing={3}>
                            {/* Chart */}
                            <Grid item xs={12} md={8} lg={9}>
                                <Paper className={fixedHeightPaper}>
                                </Paper>
                            </Grid>
                            {/* Recent Deposits */}
                            <Grid item xs={12} md={4} lg={3}>
                                <Paper className={fixedHeightPaper}>

                                    <div className="col-md-6 col-md-offset-3">
                                        <h1>{t('home.hi')} {user.username}!</h1>
                                        <p>{t('home.loggedIn')}</p>
                                    </div>

                                </Paper>
                            </Grid>
                            {/* Recent Orders */}
                            <Grid item xs={12}>
                                <Paper className={classes.paper}>
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>

                    {/* Footer */}
                    <footer className={classes.footer}>
                        <Container maxWidth="lg">
                            <Copyright />
                        </Container>
                    </footer>
                </main>
            </div>
        );
    }
}

HomePage.propTypes = {
    getUser: PropTypes.func,
    classes: PropTypes.object,
    user: PropTypes.object,
    users: PropTypes.object,
};

function mapState(state) {
    const {user} = state;
    return {user};
}

const actionCreators = {
    getUser: userActions.getOne,
    logout: userActions.logout,
}

const connectedHomePage = withStyles(styles)(connect(mapState, actionCreators)(withTranslation('common')(HomePage)));
export {connectedHomePage as HomePage};
