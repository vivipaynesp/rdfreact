import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { pt, en, es } from './_locales';

const options = {
    interpolation: {
        escapeValue: false, // not needed for react!!
    },
    debug: (process.env.NODE_ENV === `development`),
    lng: 'pt-BR',
    resources: {
        pt: {
            common: pt['pt-BR'],
        },
        en: {
            common: en.en,
        },
        es: {
            common: es.es,
        },
    },
    fallbackLng: 'en',
    ns: ['common'],
    defaultNS: 'common',
    react: {
        wait: false,
        bindI18n: 'languageChanged loaded',
        bindStore: 'added removed',
        nsMode: 'default',
        useSuspense: false
    },
};

i18n
// load translation using xhr -> see /public/locales
// learn more: https://github.com/i18next/i18next-xhr-backend
    .use(Backend)
    // detect user language
    // learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(LanguageDetector)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init(options);
;

export default i18n;