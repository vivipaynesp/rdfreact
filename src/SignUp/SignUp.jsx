import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import {connect} from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import {withStyles} from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import TextFieldGeneral from '../_components/textfield';
import ButtonGeneral from '../_components/button/button.general';
import Copyright from '../_components/copyright/';
import styles from './styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {getCountries, getStates, getCities, validateSponsorId} from '../_services/register.service'
import {register} from '../_actions/register.actions'
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import TranslateIcon from '@material-ui/icons/Translate';
import {LANGUAGES_LABEL} from "../_constants";
import NoSsr from "@material-ui/core/NoSsr";
import Menu from "@material-ui/core/Menu";
import {withTranslation} from "react-i18next";
import i18next from "i18next";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import moment from "moment";

class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                name: '',
                cpfCnpj: '',
                birthdate: moment(new Date()).add(1, 'days').format('YYYY-MM-DD'),
                rgRne: '',
                sex: 'M',
                identification: '',
                cep: '',
                address: '',
                number: '',
                neighborhood: '',
                complement: '',
                city: '',
                telephone1: '',
                telephone2: '',
                sponsorId: '',
                email: '',
                password2: '',
                password1: '',
                addressType: 'principal'
            },
            submitted: false,
            countries: [],
            states: [],
            cities: [],
            countryId: 31,
            stateId: '',
            sponsorValidated: false,
            termsAccepted: true,
            lng: i18next.language,
            languageMenu: null,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSponsor = this.handleSponsor.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleLanguageIconClick = this.handleLanguageIconClick.bind(this);
        this.handleLanguageMenuClose = this.handleLanguageMenuClose.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.maskCpfCnpj = this.maskCpfCnpj.bind(this);
        this.maskRgRne = this.maskRgRne.bind(this);
    }

    async componentDidMount() {
        await this.fetchCountries();
        await this.fetchStates(this.state.countryId);
    }

    async fetchCountries() {
        let countries = [], lastPage = 0, i = 1;

        do {
            const response = await getCountries(i++);
            const data = response['hydra:member'];
            lastPage = response['hydra:view']['hydra:last'].charAt(response['hydra:view']['hydra:last'].length - 1);

            countries = [...countries, ...data];
        } while (i <= lastPage);

        this.setState({...this.state, countries})
    }

    async fetchStates(countryId) {
        let states = [];

        if (countryId) {
            states = await getStates(countryId);
        }

        this.setState({...this.state, states})
    }

    async fetchCities(stateId) {
        let cities = [];

        if (stateId) {
            cities = await getCities(stateId);
        }

        this.setState({...this.state, cities})
    }

    handleChange = event => {
        const {name, value} = event.target;
        const {user} = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    };

    async handleSponsor(e) {
        const {value} = e.target;
        const sponsorValidated = await validateSponsorId(value);
        this.setState({...this.state, sponsorValidated})
    }

    handleSubmit = event => {
        event.preventDefault();
        this.setState({submitted: true});
        const {user, sponsorValidated, termsAccepted} = this.state;

        if (user.name && (sponsorValidated || !user.sponsorId) && user.city && user.email && user.password1 && user.password1 === user.password2 && termsAccepted) {
            const data = {...user, password: user.password1};
            delete data.password1;
            delete data.password2;
            this.props.register(data);
        }
    };

    renderCountryOptions() {
        const {countries} = this.state;

        return countries
            .sort((a, b) => {
                return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            })
            .map((country, i) => (
                <MenuItem key={i} value={country.id}>{country.name}</MenuItem>
            ));
    }

    renderStateOptions() {
        const {states} = this.state;

        return states
            .sort((a, b) => {
                return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            })
            .map((state, i) => (
                <MenuItem key={i} value={state.id}>{state.name}</MenuItem>
            ));
    }

    renderCityOptions() {
        const {cities} = this.state;

        return cities
            .sort((a, b) => {
                return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            })
            .map((city, i) => (
                <MenuItem key={i} value={city.id}>{city.name}</MenuItem>
            ));
    }

    /* Generic text mask component */
    TextMask(props) {
        const {inputRef, ...other} = props;

        return (
            <MaskedInput
                {...other}
                ref={ref => {
                    inputRef(ref ? ref.inputElement : null);
                }}
                placeholderChar={'\u2000'}
                showMask
            />
        );
    }

    /* Mask for CPF and CNPJ */
    maskCpfCnpj = rawValue => {
        if ((rawValue.match(/\d/g) || []).length <= 11) {
            return [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]
        } else {
            return [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
        }
    };

    /* Mask for RG and RNE */
    maskRgRne = rawValue => {
        if ((rawValue.match(/\d/g) || []).length <= 9 && (!/^[a-zA-Z]/.test(rawValue))) {
            return [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/]
        } else {
            return [/[a-zA-Z]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /[a-zA-Z]/]
        }
    };

    handleLanguageIconClick = event => {
        this.setState({languageMenu: event.currentTarget});
    };

    handleLanguageMenuClose = lng => {
        this.setState({languageMenu: null});
        this.setState({lng: lng}, () => {
            i18next.changeLanguage(lng);
        });
    };

    /* KeyboardDatePicker returns date - need to add 1 day to fix glitch */
    handleDateChange = date => {
        const {user} = this.state;
        this.setState({
            user: {
                ...user,
                birthdate: moment(date).add(1, 'days').format('YYYY-MM-DD')
            }
        });
    };

    render() {
        const {classes, t} = this.props;
        const {user, countryId, languageMenu, lng, stateId, sponsorValidated, submitted, termsAccepted} = this.state;

        return (
            <React.Fragment>

                {/* Menu */}
                <AppBar position="static" color="primary" elevation={0}>
                    <Toolbar variant="dense">
                        <Tooltip title={t('defaults.changeLanguage')} enterDelay={300}>
                            <Button
                                color="inherit"
                                aria-owns={languageMenu ? 'language-menu' : undefined}
                                aria-haspopup="true"
                                aria-label={t('defaults.changeLanguage')}
                                onClick={this.handleLanguageIconClick}
                                data-ga-event-category="AppBar"
                                data-ga-event-action="language"
                                className={classes.menuButton}
                            >
                                <TranslateIcon/>
                                <span className={classes.language}>
                                    {lng === 'aa' ? 'Translating' : LANGUAGES_LABEL.filter(language => language.code === lng)[0].text}
                                </span>
                                <ExpandMoreIcon fontSize="small"/>
                            </Button>
                        </Tooltip>
                        <NoSsr>
                            <Menu
                                id="language-menu"
                                anchorEl={languageMenu}
                                open={Boolean(languageMenu)}
                                onClose={this.handleLanguageMenuClose}
                            >
                                {LANGUAGES_LABEL.map(language => (
                                    <MenuItem
                                        data-no-link="true"
                                        key={language.code}
                                        selected={lng === language.code}
                                        onClick={this.handleLanguageMenuClose.bind(this, language.code)}
                                        lang={language.code}
                                    >
                                        {language.text}
                                    </MenuItem>
                                ))}
                            </Menu>
                        </NoSsr>
                    </Toolbar>
                </AppBar>

                {/* Main container */}
                <Container component="main" maxWidth="md">
                    <CssBaseline/>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            {t('signup.signUp')}
                        </Typography>
                        <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
                            <Grid container spacing={2}>

                                <Box mt={2}>
                                    <Typography component="h1" variant="h5">
                                        {t('signup.registrationData')}
                                    </Typography>
                                </Box>

                                <Grid item xs={12}>
                                    <TextFieldGeneral
                                        autoComplete="name"
                                        label={t('signup.name')}
                                        name="name"
                                        type="name"
                                        value={user.name}
                                        isRequired={true}
                                        handleChange={this.handleChange}/>

                                    {
                                        submitted && !user.name &&
                                        <FormHelperText
                                            className={classes.helper}>{t('signup.error.name')}</FormHelperText>
                                    }
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="cpfCnpj"
                                                    shrink={true}>{t('signup.cpfcnpj')}</InputLabel>
                                        <Input
                                            name="cpfCnpj"
                                            id="cpfCnpj"
                                            value={user.cpfCnpj}
                                            onChange={this.handleChange}
                                            inputComponent={this.TextMask}
                                            inputProps={{
                                                mask: this.maskCpfCnpj
                                            }}
                                        />
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                            id='birthdate'
                                            name='birthdate'
                                            label={t('signup.birthDate')}
                                            format='yyyy-MM-dd'
                                            value={user.birthdate}
                                            onChange={this.handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            className={classes.datepicker}
                                        />
                                    </MuiPickersUtilsProvider>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="rgRne"
                                                    shrink={true}>{t('signup.rg')}</InputLabel>
                                        <Input
                                            name="rgRne"
                                            id="rgRne"
                                            value={user.rgRne}
                                            onChange={this.handleChange}
                                            inputComponent={this.TextMask}
                                            inputProps={{
                                                mask: this.maskRgRne
                                            }}
                                        />
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required={true}>
                                        <InputLabel htmlFor="sex">{t('signup.sex')}</InputLabel>

                                        <Select
                                            id="sex"
                                            name="sex"
                                            value={user.sex}
                                            onChange={this.handleChange}
                                        >
                                            <MenuItem value={'M'}>{t('signup.male')}</MenuItem>
                                            <MenuItem value={'F'}>{t('signup.female')}</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>

                                <Box mt={2}>
                                    <Typography component="h1" variant="h5">
                                        {t('signup.address')}
                                    </Typography>
                                </Box>

                                <Grid item xs={12}>
                                    <TextFieldGeneral
                                        id="identification"
                                        name="identification"
                                        label={t('signup.identification.label')}
                                        placeholder={t('signup.identification.placeholder')}
                                        value={user.identification}
                                        handleChange={this.handleChange}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="cep"
                                                    shrink={true}>{t('signup.zipcode')}</InputLabel>
                                        <Input
                                            name="cep"
                                            id="cep"
                                            value={user.cep}
                                            onChange={this.handleChange}
                                            inputComponent={this.TextMask}
                                            inputProps={{
                                                mask: [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]
                                            }}
                                        />
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel id="demo-simple-select-label">{t('signup.addressType')}</InputLabel>
                                        <Select
                                            id="addressType"
                                            name="addressType"
                                            value={user.addressType}
                                            onChange={this.handleChange}
                                        >
                                            <MenuItem value={'principal'}>Principal</MenuItem>
                                            <MenuItem value={'mailing'}>Mailing</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={8}>
                                    <TextFieldGeneral
                                        id="address"
                                        name="address"
                                        label={t('signup.address')}
                                        autoComplete="billing address-level1"
                                        value={user.address}
                                        handleChange={this.handleChange}
                                        isRequired={true}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={4}>
                                    <TextFieldGeneral
                                        id="number"
                                        name="number"
                                        label={t('signup.number')}
                                        value={user.number}
                                        handleChange={this.handleChange}
                                        isRequired={true}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <TextFieldGeneral
                                        id="neighborhood"
                                        name="neighborhood"
                                        label={t('signup.neighborhood')}
                                        value={user.neighborhood}
                                        handleChange={this.handleChange}
                                        isRequired={true}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <TextFieldGeneral
                                        id="complement"
                                        name="complement"
                                        label={t('signup.complement')}
                                        value={user.complement}
                                        handleChange={this.handleChange}
                                    />
                                </Grid>

                                <Grid item xs={12}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="country">{t('signup.country')}</InputLabel>
                                        <Select
                                            value={countryId}
                                            onChange={async e => {
                                                const countryId = e.target.value;

                                                this.setState({
                                                    ...this.state,
                                                    countryId,
                                                    states: [],
                                                    cities: [],
                                                    stateId: '',
                                                    user: {
                                                        ...user,
                                                        city: ''
                                                    }
                                                });
                                                await this.fetchStates(countryId);
                                            }}
                                            inputProps={{
                                                name: 'country',
                                                id: 'country',
                                            }}
                                        >
                                            {this.renderCountryOptions()}
                                        </Select>

                                        {
                                            submitted && !countryId &&
                                            <FormHelperText className={classes.helper}>
                                                {t('signup.error.country')}
                                            </FormHelperText>
                                        }
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="state">{t('signup.state')}</InputLabel>
                                        <Select
                                            value={stateId}
                                            onChange={async e => {
                                                const stateId = e.target.value;

                                                this.setState({
                                                    ...this.state,
                                                    stateId,
                                                    cities: [],
                                                    user: {...user, city: ''}
                                                });
                                                await this.fetchCities(stateId);
                                            }}
                                            inputProps={{
                                                name: 'state',
                                                id: 'state',
                                            }}
                                        >
                                            {this.renderStateOptions()}
                                        </Select>

                                        {
                                            submitted && !stateId &&
                                            <FormHelperText className={classes.helper}>
                                                {t('signup.error.state')}
                                            </FormHelperText>
                                        }
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="city">{t('signup.city')}</InputLabel>
                                        <Select
                                            value={user.city}
                                            onChange={this.handleChange}
                                            inputProps={{
                                                name: 'city',
                                                id: 'city',
                                            }}
                                        >
                                            {this.renderCityOptions()}
                                        </Select>

                                        {
                                            submitted && !user.city &&
                                            <FormHelperText className={classes.helper}>
                                                {t('signup.error.city')}
                                            </FormHelperText>
                                        }
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth required>
                                        <InputLabel htmlFor="telephone1"
                                                    shrink={true}>{t('signup.telephone1')}</InputLabel>
                                        <Input
                                            name="telephone1"
                                            id="telephone1"
                                            value={user.telephone1}
                                            onChange={this.handleChange}
                                            inputComponent={this.TextMask}
                                            inputProps={{
                                                mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
                                            }}
                                        />
                                    </FormControl>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <FormControl fullWidth>
                                        <InputLabel htmlFor="telephone2"
                                                    shrink={true}>{t('signup.telephone2')}</InputLabel>
                                        <Input
                                            name="telephone2"
                                            id="telephone2"
                                            value={user.telephone2}
                                            onChange={this.handleChange}
                                            inputComponent={this.TextMask}
                                            inputProps={{
                                                mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
                                            }}
                                        />
                                    </FormControl>
                                </Grid>

                                <Box mt={2}>
                                    <Typography component="h1" variant="h5">
                                        {t('signup.sponsor')}
                                    </Typography>
                                </Box>

                                <Grid item xs={12}>
                                    <FormControl fullWidth>
                                        <InputLabel htmlFor="sponsorId" shrink={true}>{t('signup.sponsor')}</InputLabel>
                                        <Input
                                            name="sponsorId"
                                            id="sponsorId"
                                            value={user.sponsorId}
                                            onChange={this.handleChange}
                                            onBlur={this.handleSponsor}
                                            inputComponent={this.TextMask}
                                            inputProps={{
                                                mask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
                                            }}
                                        />

                                        {
                                            submitted && !!user.sponsorId.replace(/\D/g, '') && !sponsorValidated &&
                                            <FormHelperText
                                                className={classes.helper}>{t('signup.error.sponsor')}</FormHelperText>
                                        }
                                    </FormControl>
                                </Grid>

                                <Box mt={2}>
                                    <Typography component="h1" variant="h5">
                                        {t('signup.login')}
                                    </Typography>
                                </Box>

                                <Grid item xs={12}>
                                    <TextFieldGeneral
                                        autoComplete="email"
                                        label={t('signup.email')}
                                        name="email"
                                        type="email"
                                        value={user.email}
                                        isRequired={true}
                                        handleChange={this.handleChange}/>

                                    {
                                        submitted && !user.email &&
                                        <FormHelperText
                                            className={classes.helper}>{t('signup.error.email')}</FormHelperText>
                                    }

                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <TextFieldGeneral
                                        autoComplete="current-password"
                                        label={t('signup.password')}
                                        margin="none"
                                        name="password1"
                                        id="password1"
                                        type="password"
                                        value={user.password1}
                                        isRequired={true}
                                        handleChange={this.handleChange}/>

                                    {
                                        submitted && !user.password1 &&
                                        <FormHelperText className={classes.helper}>
                                            {t('signup.error.password')}
                                        </FormHelperText>
                                    }

                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <TextFieldGeneral
                                        autoComplete="current-password"
                                        label={t('signup.passwordConfirm')}
                                        margin="none"
                                        name="password2"
                                        id="password2"
                                        type="password"
                                        value={user.password2}
                                        isRequired={true}
                                        handleChange={this.handleChange}/>

                                    {
                                        submitted && !user.password1 &&
                                        <FormHelperText className={classes.helper}>
                                            {t('signup.error.password')}
                                        </FormHelperText>
                                    }
                                    {
                                        submitted &&
                                        user.password1 &&
                                        user.password1 !== user.password2 &&
                                        (
                                            <FormHelperText className={classes.helper}>
                                                {t('signup.error.passwordMatch')}
                                            </FormHelperText>
                                        )
                                    }

                                </Grid>
                                <Grid item xs={12}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="termsAccepted"
                                                value="termsAccepted"
                                                checked={termsAccepted}
                                                onChange={e =>
                                                    this.setState({
                                                        ...this.state,
                                                        termsAccepted: e.target.checked
                                                    })
                                                }
                                                color="primary"/>
                                        }
                                        label={t('signup.legal')}
                                    />

                                    {
                                        submitted && !termsAccepted &&
                                        <FormHelperText
                                            className={classes.helper}>{t('signup.terms')}
                                        </FormHelperText>
                                    }
                                </Grid>
                            </Grid>

                            <ButtonGeneral type="submit" value="Submit" message={t('signup.signUp')}/>

                            <Grid container justify="flex-end">
                                <Grid item>
                                    <Link href={'/login'} variant="body2">
                                        {t('signup.accountExist')}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>

                    {/* Footer */}
                    <footer className={classes.footer}>
                        <Container maxWidth="lg">
                            <Copyright/>
                        </Container>
                    </footer>
                </Container>
            </React.Fragment>
        );
    }
}

SignUp.propTypes = {
    register: PropTypes.func,
    classes: PropTypes.object,
};


const mapStateToProps = ({registration}) => ({
    registration
});

const mapDispatchToProps = {
    register
};

const connected = connect(mapStateToProps, mapDispatchToProps)(withTranslation('common')(SignUp));

export default withStyles(styles)(connected);
