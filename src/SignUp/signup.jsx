import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Copyright from '../_components/copyright/';
import Typography from '@material-ui/core/Typography';
import Loginform from './loginform';
import Profileform from './profileform';
import Addressform from './addressform';
import Review from './review';
import Container from "@material-ui/core/Container";
import moment from "moment";
import {useTranslation} from "react-i18next";
import Tooltip from "@material-ui/core/Tooltip";
import TranslateIcon from "@material-ui/core/SvgIcon/SvgIcon";
import {LANGUAGES_LABEL} from "../_constants";
import NoSsr from "@material-ui/core/NoSsr";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import {getCities, getCountries, getStates} from "../_services/register.service";

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(800 + theme.spacing(2) * 2)]: {
            width: 800,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    language: {
        margin: theme.spacing(0, 0.5, 0, 1),
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'block',
        },
    },
    menuButton: {
        marginLeft: 'auto'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
}));

const steps = ['Login', 'Profile', 'Address'];

function getStepContent(step, handleChange, handleDateChange, handleUserChange, values, renderCountryOptions, renderStateOptions, renderCityOptions) {
    switch (step) {
        case 0:
            return <Loginform handleUserChange={handleUserChange} values={values}/>;
        case 1:
            return <Profileform handleDateChange={handleDateChange} handleUserChange={handleUserChange}
                                values={values}/>;
        case 2:
            return <Addressform handleUserChange={handleUserChange} values={values}
                                renderCountryOptions={renderCountryOptions} renderStateOptions={renderStateOptions}
                                handleChange={handleChange} renderCityOptions={renderCityOptions}/>;
        case 3:
            return <Review handleUserChange={handleUserChange} values={values}/>;
        default:
            throw new Error('Unknown step');
    }
}

export default function Checkout() {
    const {t, i18n} = useTranslation();
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const [cities, setCities] = useState([]);
    const [states, setStates] = useState([]);
    const [countries, setCountries] = useState([]);
    const [submitted, setSubmitted] = useState(false);
    const [values, setValues] = useState({
        user: {
            name: '',
            cpfCnpj: '',
            birthdate: new Date(),
            rgRne: '',
            sex: 'M',
            identification: '',
            cep: '',
            address: '',
            number: '',
            neighborhood: '',
            complement: '',
            city: '',
            telephone1: '',
            telephone2: '',
            sponsorId: '1111111111',
            email: '',
            password2: '',
            password1: '',
            addressType: 'principal'
        },
        countryId: 31,
        stateId: '',
        sponsorValidated: false,
        termsAccepted: true,
        lng: i18n.language,
        languageMenu: null,
    });

    useEffect(() => {
        const controller = new AbortController();

        const fetchCountries = async () => {
            try {
                let countries = [], lastPage = 0, i = 1;

                do {
                    const response = await getCountries(i++);
                    const data = response['hydra:member'];
                    lastPage = response['hydra:view']['hydra:last'].charAt(response['hydra:view']['hydra:last'].length - 1);

                    countries = [...countries, ...data];
                } while (i <= lastPage);

                setCountries(countries);
            } catch (err) {
                if (err.name === 'AbortError') {
                    console.log("Request was canceled via controller.abort");
                    return;
                }
                // handle other errors here
            }
        };

        const fetchStates = async () => {
            try {
                let states = [];

                if (values.countryId) {
                    states = await getStates(values.countryId);
                }

                setStates(states);
            } catch (err) {
                if (err.name === 'AbortError') {
                    console.log("Request was canceled via controller.abort");
                    return;
                }
                // handle other errors here
            }
        };

        const fetchCities = async () => {
            try {
                let cities = [];

                if (values.stateId) {
                    cities = await getCities(values.stateId);
                }

                setCities(cities);
            } catch (err) {
                if (err.name === 'AbortError') {
                    console.log("Request was canceled via controller.abort");
                    return;
                }
                // handle other errors here
            }
        };

        fetchCountries();
        fetchStates();
        fetchCities();

        // Cleanup function that will be called on
        // 1. Unmount
        // 2. Dependency Array Change
        return () => {
            controller.abort();
        }
    }, [setCountries, setStates, setCities, values.countryId, values.stateId]);

    const renderCountryOptions = () => {
        return countries
            .sort((a, b) => {
                return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            })
            .map((country, i) => (
                <MenuItem key={i} value={country.id}>{country.name}</MenuItem>
            ));
    };

    const renderStateOptions = () => {
        return states
            .sort((a, b) => {
                return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            })
            .map((state, i) => (
                <MenuItem key={i} value={state.id}>{state.name}</MenuItem>
            ));
    };

    const renderCityOptions = () => {
        return cities
            .sort((a, b) => {
                return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            })
            .map((city, i) => (
                <MenuItem key={i} value={city.id}>{city.name}</MenuItem>
            ));
    };

    const handleSubmit = event => {
        // event.preventDefault();
        setSubmitted(true);

        console.log(submitted);
    };

    const handleLanguageIconClick = event => {
        setValues({
            ...values,
            languageMenu: event.currentTarget,
        });
    };

    const handleLanguageMenuClose = lng => {
        i18n.changeLanguage(lng);
        setValues({
            ...values,
            languageMenu: null,
            lng,
        });
    };

    /* KeyboardDatePicker returns date - need to add 1 day to fix glitch */
    const handleDateChange = name => date => {
        setValues({
            ...values,
            user: {
                ...values.user,
                [name]: moment(date).add(1, 'days').format('YYYY-MM-DD'),
            }
        });
    };

    const handleUserChange = name => event => {
        setValues({
            ...values,
            user: {
                ...values.user,
                [name]: event.target.value,
            }
        });
    };

    const handleChange = name => event => {
        setValues(values => ({
            ...values,
            [name]: event.target.value,
        }));
    };

    const handleNext = () => {
        const {password1, password2} = values.user;

        if (password1 === password2) {
            setActiveStep(activeStep + 1);
        }
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };

    return (
        <React.Fragment>

            <CssBaseline/>

            {/* Menu */}
            <AppBar position="static" color="primary" elevation={0}>
                <Toolbar variant="dense">
                    <Tooltip title={t('defaults.changeLanguage')} enterDelay={300}>
                        <Button
                            color="inherit"
                            aria-owns={values.languageMenu ? 'language-menu' : undefined}
                            aria-haspopup="true"
                            aria-label={t('defaults.changeLanguage')}
                            onClick={handleLanguageIconClick}
                            data-ga-event-category="AppBar"
                            data-ga-event-action="language"
                            className={classes.menuButton}
                        >
                            <TranslateIcon/>
                            <span className={classes.language}>
                                    {values.lng === 'aa' ? 'Translating' : LANGUAGES_LABEL.filter(language => language.code === values.lng)[0].text}
                                </span>
                            <ExpandMoreIcon fontSize="small"/>
                        </Button>
                    </Tooltip>
                    <NoSsr>
                        <Menu
                            id="language-menu"
                            anchorEl={values.languageMenu}
                            open={Boolean(values.languageMenu)}
                            onClose={handleLanguageMenuClose}
                        >
                            {LANGUAGES_LABEL.map(language => (
                                <MenuItem
                                    data-no-link="true"
                                    key={language.code}
                                    selected={values.lng === language.code}
                                    onClick={() => handleLanguageMenuClose(language.code)}
                                    lang={language.code}
                                >
                                    {language.text}
                                </MenuItem>
                            ))}
                        </Menu>
                    </NoSsr>
                </Toolbar>
            </AppBar>

            {/* Main */}
            <main className={classes.layout}>

                <Paper className={classes.paper}>

                    <Typography component="h1" variant="h4" align="center">
                        {t('signup.accountCreation')}
                    </Typography>

                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map(label => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>

                    <React.Fragment>
                        {activeStep === steps.length ? (
                            <React.Fragment>
                                <Typography variant="h5" gutterBottom>
                                    Thank you for your order.
                                </Typography>
                                <Typography variant="subtitle1">
                                    Your order number is #2001539. We have emailed your order confirmation, and will
                                    send you an update when your order has shipped.
                                </Typography>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>

                                {getStepContent(activeStep, handleChange, handleDateChange, handleUserChange, values, renderCountryOptions, renderStateOptions, renderCityOptions)}

                                <div className={classes.buttons}>
                                    {activeStep !== 0 && (
                                        <Button onClick={handleBack} className={classes.button}>
                                            Back
                                        </Button>
                                    )}
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={activeStep === steps.length - 1 ? handleSubmit : handleNext}
                                        className={classes.button}
                                        // type={activeStep === steps.length - 1 ? "submit" : null}
                                    >
                                        {activeStep === steps.length - 1 ? t('signup.register') : t('signup.next')}
                                    </Button>
                                </div>

                            </React.Fragment>
                        )}

                        <React.Fragment>

                            <Grid container justify="flex-end">
                                <Grid item xs={12}>
                                    <Link href={'/login'} variant="body2">
                                        {t('signup.accountExist')}
                                    </Link>
                                </Grid>
                            </Grid>

                        </React.Fragment>

                    </React.Fragment>

                </Paper>

            </main>

            {/* Footer */}
            <footer className={classes.footer}>
                <Container maxWidth="lg">
                    <Copyright/>
                </Container>
            </footer>

        </React.Fragment>
    );
}