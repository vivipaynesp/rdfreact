import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {useTranslation} from "react-i18next";
import TextFieldGeneral from "../_components/textfield";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import MaskedInput from "react-text-mask";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

const useStyles = makeStyles(theme => ({
    datepicker: {
        width: '100%'
    },
    helper: {
        color: theme.palette.secondary.main,
    },
}));

function textMaskCustom(props) {
    const {inputRef, ...other} = props;

    return (
        <MaskedInput
            {...other}
            ref={ref => {
                inputRef(ref ? ref.inputElement : null);
            }}
            placeholderChar={'\u2000'}
            showMask
        />
    );
}

textMaskCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
};

/* Mask for CPF and CNPJ */
function maskCpfCnpj(rawValue) {
    if ((rawValue.match(/\d/g) || []).length <= 11) {
        return [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]
    } else {
        return [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
    }
}

/* Mask for RG and RNE */
function maskRgRne(rawValue) {
    if ((rawValue.match(/\d/g) || []).length <= 9 && (!/^[a-zA-Z]/.test(rawValue))) {
        return [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/]
    } else {
        return [/[a-zA-Z]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /[a-zA-Z]/]
    }
}

export default function Profileform(props) {
    const {t} = useTranslation();
    const classes = useStyles();
    const {user} = props.values;
    const {handleUserChange, handleDateChange} = props;

    return (
        <React.Fragment>

            <Typography variant="h6" gutterBottom>
                {t('signup.profileInformation')}
            </Typography>

            <Grid container spacing={3}>

                <Grid item xs={12} sm={9}>
                    <TextFieldGeneral
                        id="name"
                        autoComplete="name"
                        label={t('signup.name')}
                        name="name"
                        type="name"
                        value={user.name}
                        isRequired={true}
                        onChange={handleUserChange('name')}/>

                    {
                        !user.name &&
                        <FormHelperText
                            className={classes.helper}>{t('signup.error.name')}</FormHelperText>
                    }
                </Grid>

                <Grid item xs={12} sm={3}>
                    <FormControl fullWidth required={true}>
                        <InputLabel htmlFor="sex">{t('signup.sex')}</InputLabel>

                        <Select
                            id="sex"
                            name="sex"
                            value={user.sex}
                            onChange={handleUserChange('sex')}
                        >
                            <MenuItem value={'M'}>{t('signup.male')}</MenuItem>
                            <MenuItem value={'F'}>{t('signup.female')}</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="cpfCnpj"
                                    shrink={true}>{t('signup.cpfcnpj')}</InputLabel>
                        <Input
                            name="cpfCnpj"
                            id="cpfCnpj"
                            value={user.cpfCnpj}
                            onChange={handleUserChange('cpfCnpj')}
                            inputComponent={textMaskCustom}
                            inputProps={{
                                mask: maskCpfCnpj
                            }}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="rgRne"
                                    shrink={true}>{t('signup.rg')}</InputLabel>
                        <Input
                            name="rgRne"
                            id="rgRne"
                            value={user.rgRne}
                            onChange={handleUserChange('rgRne')}
                            inputComponent={textMaskCustom}
                            inputProps={{
                                mask: maskRgRne
                            }}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            id='birthdate'
                            name='birthdate'
                            label={t('signup.birthDate')}
                            format='yyyy-MM-dd'
                            value={user.birthdate}
                            onChange={handleDateChange('birthdate')}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            className={classes.datepicker}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>

            </Grid>

        </React.Fragment>
    );
}

Profileform.propTypes = {
    handleDateChange: PropTypes.func.isRequired,
    handleUserChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
};
