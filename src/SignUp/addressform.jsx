import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import {useTranslation} from 'react-i18next';
import MaskedInput from "react-text-mask";
import TextFieldGeneral from "../_components/textfield";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

function textMaskCustom(props) {
    const {inputRef, ...other} = props;

    return (
        <MaskedInput
            {...other}
            ref={ref => {
                inputRef(ref ? ref.inputElement : null);
            }}
            placeholderChar={'\u2000'}
            showMask
        />
    );
}

textMaskCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
};

export default function AddressForm(props) {
    const {t} = useTranslation();
    const {countryId, stateId, user} = props.values;
    const {handleChange, handleUserChange, renderCountryOptions, renderStateOptions, renderCityOptions} = props;

    return (
        <React.Fragment>

            <Typography variant="h6" gutterBottom>
                {t('signup.loginInformation')}
            </Typography>

            <Grid container spacing={3}>

                <Grid item xs={12}>
                    <TextFieldGeneral
                        id="identification"
                        name="identification"
                        label={t('signup.identification.label')}
                        placeholder={t('signup.identification.placeholder')}
                        value={user.identification}
                        onChange={handleUserChange('identification')}
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="cep"
                                    shrink={true}>{t('signup.zipcode')}</InputLabel>
                        <Input
                            name="cep"
                            id="cep"
                            value={user.cep}
                            onChange={handleUserChange('cep')}
                            inputComponent={textMaskCustom}
                            inputProps={{
                                mask: [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]
                            }}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel id="demo-simple-select-label">{t('signup.addressType')}</InputLabel>
                        <Select
                            id="addressType"
                            name="addressType"
                            value={user.addressType}
                            onChange={handleUserChange('addressType')}
                        >
                            <MenuItem value={'principal'}>Principal</MenuItem>
                            <MenuItem value={'mailing'}>Mailing</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={8}>
                    <TextFieldGeneral
                        id="address"
                        name="address"
                        label={t('signup.address')}
                        autoComplete="billing address-level1"
                        value={user.address}
                        onChange={handleUserChange('address')}
                        isRequired={true}
                    />
                </Grid>

                <Grid item xs={12} sm={4}>
                    <TextFieldGeneral
                        id="number"
                        name="number"
                        label={t('signup.number')}
                        value={user.number}
                        onChange={handleUserChange('number')}
                        isRequired={true}
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextFieldGeneral
                        id="neighborhood"
                        name="neighborhood"
                        label={t('signup.neighborhood')}
                        value={user.neighborhood}
                        onChange={handleUserChange('neighborhood')}
                        isRequired={true}
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextFieldGeneral
                        id="complement"
                        name="complement"
                        label={t('signup.complement')}
                        value={user.complement}
                        onChange={handleUserChange('complement')}
                    />
                </Grid>

                <Grid item xs={12}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="country">{t('signup.country')}</InputLabel>
                        <Select
                            value={countryId}
                            onChange={handleChange('countryId')}
                            inputProps={{
                                name: 'country',
                                id: 'country',
                            }}
                        >
                            {renderCountryOptions()}
                        </Select>

                        {/*{*/}
                        {/*    submitted && !countryId &&*/}
                        {/*    <FormHelperText className={classes.helper}>*/}
                        {/*        {t('signup.error.country')}*/}
                        {/*    </FormHelperText>*/}
                        {/*}*/}
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="state">{t('signup.state')}</InputLabel>
                        <Select
                            value={stateId}
                            onChange={handleChange('stateId')}
                            inputProps={{
                                name: 'state',
                                id: 'state',
                            }}
                        >
                            {renderStateOptions()}
                        </Select>

                        {/*        {*/}
                        {/*            submitted && !stateId &&*/}
                        {/*            <FormHelperText className={classes.helper}>*/}
                        {/*                {t('signup.error.state')}*/}
                        {/*            </FormHelperText>*/}
                        {/*        }*/}
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="city">{t('signup.city')}</InputLabel>
                        <Select
                            value={user.city}
                            onChange={handleUserChange('city')}
                            inputProps={{
                                name: 'city',
                                id: 'city',
                            }}
                        >
                            {renderCityOptions()}
                        </Select>

                        {/*{*/}
                        {/*    submitted && !values.city &&*/}
                        {/*    <FormHelperText className={classes.helper}>*/}
                        {/*        {t('signup.error.city')}*/}
                        {/*    </FormHelperText>*/}
                        {/*}*/}
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth required>
                        <InputLabel htmlFor="telephone1"
                                    shrink={true}>{t('signup.telephone1')}</InputLabel>
                        <Input
                            name="telephone1"
                            id="telephone1"
                            value={user.telephone1}
                            onChange={handleUserChange('telephone1')}
                            inputComponent={textMaskCustom}
                            inputProps={{
                                mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
                            }}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl fullWidth>
                        <InputLabel htmlFor="telephone2"
                                    shrink={true}>{t('signup.telephone2')}</InputLabel>
                        <Input
                            name="telephone2"
                            id="telephone2"
                            value={user.telephone2}
                            onChange={handleUserChange('telephone2')}
                            inputComponent={textMaskCustom}
                            inputProps={{
                                mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
                            }}
                        />
                    </FormControl>
                </Grid>

            </Grid>

        </React.Fragment>
    );
}

AddressForm.propTypes = {
    handleUserChange: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    renderCityOptions: PropTypes.func.isRequired,
    renderCountryOptions: PropTypes.func.isRequired,
    renderStateOptions: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
};