import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import {useTranslation} from 'react-i18next';
import MaskedInput from "react-text-mask";
import TextFieldGeneral from "../_components/textfield";
import FormHelperText from "@material-ui/core/FormHelperText";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    helper: {
        color: theme.palette.secondary.main,
    },
}));

function textMaskCustom(props) {
    const {inputRef, ...other} = props;

    return (
        <MaskedInput
            {...other}
            ref={ref => {
                inputRef(ref ? ref.inputElement : null);
            }}
            placeholderChar={'\u2000'}
            showMask
        />
    );
}

textMaskCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
};

export default function Loginform(props) {
    const {t} = useTranslation();
    const classes = useStyles();
    const {user} = props.values;
    const {handleUserChange} = props;

    return (
        <React.Fragment>

            <Typography variant="h6" gutterBottom>
                {t('signup.loginInformation')}
            </Typography>

            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <FormControl fullWidth>
                        <InputLabel htmlFor="sponsorId" shrink={true}>{t('signup.sponsor')}</InputLabel>
                        <Input
                            id="sponsorId"
                            name="sponsorId"
                            value={user.sponsorId}
                            onChange={handleUserChange('sponsorId')}
                            inputComponent={textMaskCustom}
                            inputProps={{
                                mask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
                            }}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={12}>
                    <TextFieldGeneral
                        id="email"
                        name="email"
                        autoComplete="email"
                        label={t('signup.email')}
                        type="email"
                        value={user.email}
                        isRequired={true}
                        handleChange={handleUserChange('email')}/>

                    {
                        !user.email &&
                        <FormHelperText
                            className={classes.helper}>{t('signup.error.email')}</FormHelperText>
                    }
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextFieldGeneral
                        id="password1"
                        name="password1"
                        autoComplete="current-password"
                        label={t('signup.password')}
                        margin="none"
                        type="password"
                        value={user.password1}
                        isRequired={true}
                        handleChange={handleUserChange('password1')}/>

                    {
                        !user.password1 &&
                        <FormHelperText className={classes.helper}>
                            {t('signup.error.password')}
                        </FormHelperText>
                    }

                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextFieldGeneral
                        id="password2"
                        name="password2"
                        autoComplete="current-password"
                        label={t('signup.passwordConfirm')}
                        margin="none"
                        type="password"
                        value={user.password2}
                        isRequired={true}
                        handleChange={handleUserChange('password2')}/>

                    {
                        !user.password1 &&
                        <FormHelperText className={classes.helper}>
                            {t('signup.error.password')}
                        </FormHelperText>
                    }
                    {
                        user.password1 &&
                        user.password1 !== user.password2 &&
                        (
                            <FormHelperText className={classes.helper}>
                                {t('signup.error.passwordMatch')}
                            </FormHelperText>
                        )
                    }
                </Grid>
            </Grid>

        </React.Fragment>
    );
}

Loginform.propTypes = {
    handleUserChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
};