import {userConstants} from '../_constants';

export function user(state = {}, action) {
    switch (action.type) {
        case userConstants.GETONE_REQUEST:
            return {
                loading: true
            };
        case userConstants.GETONE_SUCCESS:
            return action.user;
        case userConstants.GETONE_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}