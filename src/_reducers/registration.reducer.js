import {actionTypes} from '../_constants/register.constants';

const initialState = {
    registering: false,
    success: false,
    failure: false
};

export function registration(state = initialState, action) {
    switch (action.type) {
        case actionTypes.REGISTER_REQUEST:
            return {
                ...state,
                registering: true,
                success: false,
                failure: false
            };
        case actionTypes.REGISTER_SUCCESS:
            return {
                ...state,
                success: true,
                registering: false
            };
        case actionTypes.REGISTER_FAILURE:
            return {
                ...state,
                failure: true,
                registering: false,
                error: action.payload
            };
        default:
            return state
    }
}