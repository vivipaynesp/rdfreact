// Valid languages to server-side render in production
const LANGUAGES = ['pt-BR', 'en', 'es', 'aa'];

// Server side rendered languages
const LANGUAGES_SSR = ['pt-BR', 'en', 'es'];

// Work in progress
const LANGUAGES_IN_PROGRESS = [...LANGUAGES];

// Valid languages to use in production
const LANGUAGES_LABEL = [
    {
        code: 'pt-BR',
        text: 'Português',
    },
    {
        code: 'en',
        text: 'English',
    },
    {
        code: 'es',
        text: 'Español',
    },
];

module.exports = {
    LANGUAGES,
    LANGUAGES_SSR,
    LANGUAGES_LABEL,
    LANGUAGES_IN_PROGRESS,
};
